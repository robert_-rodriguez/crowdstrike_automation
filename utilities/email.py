import boto3

def send_email(to_addresses, cc_addresses, from_address, subject, body):
    ses = boto3.client('ses')
    ses.send_email(Source=from_address, Destination={'ToAddresses': to_addresses, 'CcAddresses': [from_address]},
                   Message={'Subject': {'Data': subject}, 'Body': {'Text': {'Data': body}}})

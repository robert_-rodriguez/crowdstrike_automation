from os import environ
from urllib import parse

from oauthlib.oauth2 import BackendApplicationClient
from requests_oauthlib import OAuth2Session


BASE_URL = 'https://api.crowdstrike.com'
TOKEN_PATH = '/oauth2/token'
LIST_DETECTIONS_PATH = '/detects/queries/detects/v1'
GET_DETECTION_PATH = '/detects/entities/summaries/GET/v1'
GET_HOST_PATH = '/devices/entities/devices/v1'
GET_USER_PATH = '/users/entities/users/v1?'


def save_token(token):
    # not sure if this is needed with this setup
    pass


def get_client():
    client_id, client_secret = environ['CLIENT_ID'], environ['CLIENT_SECRET']
    client = BackendApplicationClient(client_id=client_id)
    oauth = OAuth2Session(client=client)
    token = oauth.fetch_token(token_url=parse.urljoin(BASE_URL, TOKEN_PATH), client_id=client_id, client_secret=client_secret)
    token['expires_in'] = '-1800'

    return OAuth2Session(client_id, token=token, auto_refresh_url=parse.urljoin(BASE_URL, TOKEN_PATH),
                         auto_refresh_kwargs={'client_id': client_id, 'client_secret': client_secret},
                         token_updater=save_token)


def get_detections(client, min_date):
    headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
    request = client.get(parse.urljoin(BASE_URL, LIST_DETECTIONS_PATH), headers=headers, params={'filter': "first_behavior: >='{}'".format(min_date.isoformat())})
    request.raise_for_status()

    detection_ids = request.json()['resources']
    print(request.text)
    request = client.get(parse.urljoin(BASE_URL, LIST_DETECTIONS_PATH), headers=headers, params={'filter': "first_behavior: >='{}'".format(min_date.isoformat()), 'start' : 10})
    if not detection_ids:
        return None

    request = client.post(parse.urljoin(BASE_URL, GET_DETECTION_PATH), headers=headers, data='{{"ids": ["{}"]}}'.format('","'.join(detection_ids)))
    request.raise_for_status()

    return request.json()['resources']


def get_device(client, device_id):
    headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
    request = client.get(parse.urljoin(BASE_URL, GET_HOST_PATH), headers=headers, data='{{"ids": ["{}"]}}'.format(device_id))

    return request.json()


def get_user(client, user_id):
    headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
    request = client.get(parse.urljoin(BASE_URL, '/users/entities/users/v1?ids=S-1-5-21-610354233-1133782292-1394453194-50886'), headers=headers)
    return request.json()

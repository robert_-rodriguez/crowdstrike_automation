from datetime import datetime, timedelta

from utilities import crowdstrike_api, email

NOW = datetime.now()
SECURITY_EMAIL_ADDRESS = 'security@slalom.com'


def handle_malware_detection(detection):
    #email attackee
    body="""Hello,
    a file you attempted to run was detected as containing malware. The name of the file is: {0}. The file name may not look familiar as it was probably run as a part of a bigger package. Crowdstrike endpoint management blocked and has quarantined this file. Please use care in which websites you download applications from.

Thanks,
Slalom Security
    """
    email_address = detection['behaviors'][0]['user_name'].split('@')[0] + '@slalom.com'
    email.send_email([email_address], [SECURITY_EMAIL_ADDRESS], SECURITY_EMAIL_ADDRESS, 'A file you attempted to run was infected with malware', body)
    #change detection status


def is_malware_with_username(detection):
    username = detection['behaviors'][0]['user_name']
    return 'pup' in detection['behaviors'][0]['technique'].lower() or detection['behaviors'][0]['tactic'].lower() == 'malware'

if __name__ == '__main__':
    client = crowdstrike_api.get_client()
    detections = crowdstrike_api.get_detections(client, NOW - timedelta(days=1000))
    malware_detections = [x for x in detections if is_malware_with_username(x)]
    print(len(malware_detections))

    for detection in malware_detections:
        handle_malware_detection(detection)
